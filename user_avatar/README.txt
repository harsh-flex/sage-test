User Avatar
===============
User Avatar: This module is build to provide a block of user images when user visits user profile page.

1. A block is created & placed in right side bar of of bartik theme.
2. This block generates a image when visits the profile page.
3. Also that particular image is attached to user_picture field.