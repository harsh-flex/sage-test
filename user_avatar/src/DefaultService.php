<?php

namespace Drupal\user_avatar;

use Drupal\Core\Routing\CurrentRouteMatch;

/**
 * Class DefaultService.
 */
class DefaultService {
  CONST IMAGEDIMENSIONS = 400;

  /**
   * Drupal\Core\Routing\CurrentRouteMatch definition.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * Constructs a new DefaultService object.
   */
  public function __construct(CurrentRouteMatch $current_route_match) {
    $this->currentRouteMatch = $current_route_match;
  }

  public function getImageFromAdorable($flag = FALSE) {
    $url = $file = '';
    if ($this->currentRouteMatch->getCurrentRouteMatch()->getRouteName() == 'entity.user.canonical') {
      // get user Email.
      $email  = $this->currentRouteMatch->getCurrentRouteMatch()->getParameter('user')->getEmail();
      // Get user id.
      $id     = $this->currentRouteMatch->getCurrentRouteMatch()->getParameter('user')->id();
      // Download Image from adorable in 200 dimensions.
      $imgSrc = 'https://api.adorable.io/avatars/' . self::IMAGEDIMENSIONS . '/' . $email . '.png';
      // Content type
//      header('Content-type: image/png');


      $img  = $this->loadPNG($imgSrc);
      imagefilter($img, IMG_FILTER_GRAYSCALE); //first, convert to grayscale
      imagefilter($img, IMG_FILTER_CONTRAST, -255); //then, apply a full contrast
      ob_start();
      $img  = imagescale($img, 200, 200);
      imagepng($img);
      $data = ob_get_clean();
      $path = 'public://useravatar/';
      
      $file = $this->getFile($path, $data, $id);
      if ($flag) {
        return $file;
      }
      if ($file) {
        // Get Image url.
        $url = file_create_url($file->getFileUri());
      }
    }
    return $url;
  }

  /**
   * Function to load image fromadorable.
   *
   * @param type $imgname
   *   Image path of adorable.
   *
   * @return string
   */
  public function loadPNG($imgname) {
    /* Attempt to open */
    $im = @imagecreatefrompng($imgname);
    /* See if it failed */
    if (!$im) {
      /* Create a blank image */
      $im  = imagecreatetruecolor(200, 200);
      $bgc = imagecolorallocate($im, 255, 255, 255);
      $tc  = imagecolorallocate($im, 0, 0, 0);

      imagefilledrectangle($im, 0, 0, 150, 30, $bgc);

      /* Output an error message */
      imagestring($im, 1, 5, 5, 'Error loading ' . $imgname, $tc);
    }
    return $im;
  }

  public function getFile($path, $data, $id) {
    // Create Directory.
    if (file_prepare_directory($path, FILE_CREATE_DIRECTORY)) {
      $file    = file_save_data($data, $path . $id . "-file.png",
          $replace = FILE_EXISTS_REPLACE);
      return $file;
    }
    return FALSE;
  }

}