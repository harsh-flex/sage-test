<?php

namespace Drupal\user_avatar\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\user_avatar\DefaultService;
use Drupal\Core\Routing\CurrentRouteMatch;

/**
 * Provides a 'UserAvatarBlock' block.
 *
 * @Block(
 *  id = "user_avatar_block",
 *  admin_label = @Translation("User avatar block"),
 * )
 */
class UserAvatarBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Routing\CurrentRouteMatch definition.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRoute;

  /**
   * Drupal\user_avatar\DefaultService definition.
   *
   * @var \Drupal\user_avatar\DefaultService
   */
  protected $userAvatar;

  /**
   * Constructs a new UserAvatarBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(
  array $configuration, $plugin_id, $plugin_definition,
  CurrentRouteMatch $currentRoute,
      DefaultService $userAvatar
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentRoute      = $currentRoute;
    $this->userAvatar = $userAvatar;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
                                array $configuration, $plugin_id,
                                $plugin_definition) {
    return new static(
        $configuration, $plugin_id, $plugin_definition,
        $container->get('current_route_match'),
        $container->get('user_avatar.default')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $url = $this->userAvatar->getImageFromAdorable();
    
    $build = [];
    $build = [
      '#theme' => 'user_avatar',
      '#imgsrc' => $url,
      '#cache' => ['contexts' => ['url.path']],
    ];
    return $build;
  }

}